{
    description = "My fuel service backend";

    inputs = {
        nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
        utils.url = "github:numtide/flake-utils";
    };

    outputs = { self, nixpkgs, utils }: utils.lib.eachDefaultSystem (system:
    let
        pkgs = nixpkgs.legacyPackages.${system};
    in rec {

        devShell = pkgs.mkShell {
            nativeBuildInputs = with pkgs; [ rustc cargo ];
        };
    });
}